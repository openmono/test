# Test of GitLab

[ ] To decide whether to use GitLab.com for hosting OpenMono projects,
    as a developer, I want to make a proof of concept of letting GitLab
    run continuous integration for OS X, Windows, and GNU/Linux.
